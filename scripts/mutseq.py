#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Copyright (C) 2015 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import collections
import csv
import argparse
import logging
import os
import subprocess
import sys

import mutseq.align
import mutseq.genetic
import mutseq.report

def main(argv=None):
    if not argv:
        argv = sys.argv
    parser = argparse.ArgumentParser(description='Prediction of sgRNA targets.')
    parser.add_argument('-l', '--path_loci', dest='path_loci', action='store', default='loci.csv', help='Path to loci CSV file.')
    parser.add_argument('-i', '--path_index', dest='path_index', action='store', default='.', help='Path to mapping index.')
    parser.add_argument('-d', '--path_data', dest='path_data', action='store', default='.', help='Path to sequencing data.')
    parser.add_argument('-c', '--cat_cmd', dest='cat_cmd', action='store', default='zcat', help='Program to stream data to mapped (cat, zcat or bzcat etc).')
    parser.add_argument('-t', '--trim_max', dest='trim_max', action='store', type=int, default=3, help='Maximum length of PCR oligo trimming.')
    parser.add_argument('-a', '--min_padding', dest='min_padding', action='store', type=int, default=6, help='Minimum length of alignment in addition to PCR oligo.')
    parser.add_argument('-n', '--min_allele_percent', dest='min_allele_percent', action='store', type=float, default=1., help='Minimum percentage of reads to report an allele.')
    parser.add_argument('-r', '--min_allele_reads', dest='min_allele_reads', action='store', type=int, default=1000, help='Minimum number of reads to report an allele.')
    parser.add_argument('-o', '--report', dest='report', action='store', default='-', help='Path to report file or "-" for stdout.')
    parser.add_argument('-g', '--log', dest='log', action='store', default='INFO', help='Log level: INFO, DEBUG etc.')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='Verbose.')
    parser.add_argument('-p', '--processor', dest='num_processor', action='store', type=int, default=1, help='Number of processor.')
    args = parser.parse_args(argv[1:])

    # Logging
    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%H:%M', level=args.log)

    # Parsing loci CSV file
    loci = []
    Locus = collections.namedtuple('Locus', ['name', 'samples', 'oligo_fw', 'oligo_rv', 'seq'])
    with open(args.path_loci, newline='') as csvfile:
        # Skipping header
        next(csvfile, None)
        # Read loci
        for row in csv.reader(csvfile):
            row[1] = row[1].split(',')
            loci.append(Locus(*row))
    # Samples
    samples = []
    for sample in [sample for locus in loci for sample in locus.samples]:
        if sample not in samples:
            samples.append(sample)
    logging.info('Parsing %s: %i loci and %i sample(s) found.'%(args.path_loci, len(loci), len(samples)))

    # Creating index
    if os.path.exists(os.path.join(args.path_index, 'index')):
        logging.info('Index already exists in %s'%(args.path_index))
    else:
        logging.info('Creating index in %s'%(args.path_index))
        # Creating FASTA
        with open(os.path.join(args.path_index, 'seq.fa'), 'wt') as f:
            for locus in loci:
                f.write('>%s\n%s\n'%(locus.name, locus.seq))
        # Creating index
        with open('index.log', 'wt') as f:
            subprocess.check_call(['gmap_build', '--kmer', '10', '--dir', '.', '--db', 'index', 'seq.fa'], stdout=subprocess.DEVNULL, stderr=f, cwd=args.path_index)

    # Mapping & Allele detection
    for sample in samples:
        # Mapping and aligning reads
        path_sample = os.path.join(args.path_data, sample)
        if os.path.exists(sample + '_R1.sam'):
            logging.info('%s already aligned'%(sample))
        else:
            logging.info('Aligning %s'%(sample))
            mutseq.align.align_gmap(sample,
                                    path_in = args.path_data,
                                    gmap_index_path = args.path_index,
                                    gmap_index = 'index',
                                    cat_cmd = args.cat_cmd,
                                    num_processor = args.num_processor)

        # Allele detection
        logging.info('Detecting allele in %s'%(sample))
        loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned = mutseq.genetic.get_alleles_per_loci(sample, loci, args.trim_max, args.min_padding, args.verbose)

        # Report
        logging.info('Writing report in %s'%(args.report))
        if args.report == '-':
            fout = sys.stdout
        else:
            fout = open(args.report, 'at')
        mutseq.report.write_report(sample, loci, fout, loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned, args.min_allele_percent, args.min_allele_reads)
        # Closing
        if args.report != '-':
            fout.close()

if __name__ == '__main__':
    sys.exit(main())
