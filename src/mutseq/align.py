# -*- coding: utf-8 -*-

#
# Copyright (C) 2015 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import glob
import logging
import os
import subprocess

def align_gmap(sample, path_in, gmap_index_path, gmap_index, cat_cmd=None, logger=None, num_processor=1):
    # Parameters
    if logger is None:
        logger = logging
    else:
        logger = logger.getChild(logger.name+'.align_gmap')

    for read in ['R1', 'R2']:
        cmd = ['gmap',
               '--dir', gmap_index_path,
               '--db', gmap_index,
               '--min-intronlength', '1600',
               '--allow-close-indels', '2',
               '--npaths', '1',
               '--ordered',
               '--input-buffer-size', '12000',
               '--batch', '4',
               '--format', 'samse',
               '--nthreads', str(num_processor)]
        # Input
        if cat_cmd is not None:
            path_fins = glob.glob(os.path.join(path_in, sample, '*_%s_*.fastq*'%read))
            if len(path_fins) == 0:
                raise Exception('No input file found')
            logger.info('Loading file(s): %s'%(','.join(path_fins)))
            catp = subprocess.Popen([cat_cmd] + path_fins, stdout=subprocess.PIPE)
            gmap_in = catp.stdout
        else:
            catp = None
            gmap_in = open(os.path.join(path_in, sample+'.fastq'), 'rt')
        # Output
        with open('%s_%s.sam'%(sample, read), 'wt') as fout, open('%s_%s.log'%(sample, read), 'wt') as ferr:
            logger.info('Starting GMAP (%s,%s) with %s'%(sample, read, cmd))
            p = subprocess.Popen(cmd, stdin=gmap_in, stdout=fout, stderr=ferr)
            if catp is not None:
                catp.wait()
            p.wait()
            if p.returncode != 0:
                raise subprocess.SubprocessError('GMAP failed')
