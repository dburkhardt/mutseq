# -*- coding: utf-8 -*-

#
# Copyright (C) 2015 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import logging
import math

from . import aln_sam

def is_wt(read, trim_max=3):
    if len(read.cigar) == 1 and read.cigar[0][0] == 'M':
        return True
    if len(read.cigar) == 2:
        if (read.cigar[0][0] == 'S' and read.cigar[0][1] <= trim_max and read.cigar[1][0] == 'M') \
           or (read.cigar[0][0] == 'M' and read.cigar[1][0] == 'S' and read.cigar[1][1] <= trim_max):
           return True
    return False

def get_indels(read):
    indels = []
    current_rpos = read.pos
    for iop in range(len(read.cigar) - 1):
        if read.cigar[iop][0] == 'D' or read.cigar[iop][0] == 'I':
            indels.append((read.cigar[iop][0], current_rpos, current_rpos+read.cigar[iop][1]))
        # Soft (S) and hard (H) clipping are not included in pos
        if read.cigar[iop][0] == 'M' or read.cigar[iop][0] == 'D':
            current_rpos += read.cigar[iop][1]
    return indels

def get_alleles_per_loci(sample, loci, trim_max, min_padding, verbose):
    # Init. stats per locus
    loci_in_samples = [locus for locus in loci if sample in locus.samples]
    loci_alleles = {locus.name: {} for locus in loci_in_samples}
    loci_npair = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_mutant = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_aligned = {locus.name: 0 for locus in loci_in_samples}
    loci_npair_wt = {locus.name: 0 for locus in loci_in_samples}
    npair_unaligned = 0
    # Loci oligo lengths
    loci_oligo_lengths = {locus.name: (len(locus.oligo_fw), len(locus.oligo_rv)) for locus in loci if sample in locus.samples}
    # Loci sequence lengths
    loci_seq_lengths = {locus.name: len(locus.seq) for locus in loci if sample in locus.samples}

    # Looping over alignments
    with open(sample + '_R1.sam', 'rt') as fr1, open(sample + '_R2.sam', 'rt') as fr2:
        for l1, l2 in zip(fr1, fr2):
            if l1[0].startswith('@'):
                continue

            # Parsing SAM
            r1 = aln_sam.AlignedRead(l1)
            r2 = aln_sam.AlignedRead(l2)

            # Locus
            if r1.rname != '*':
                locus_name = r1.rname
            elif r2.rname != '*':
                locus_name = r2.rname
            else:
                npair_unaligned += 1
                continue

            # Error in input
            if r1.qname != r2.qname:
                logging.error('Read1 and read2 name are different: %s != %s', r1.qname, r2.qname)
                continue
            # Read1 and read2 align on two different loci
            if r1.is_mapped() and r2.is_mapped() and r1.rname != r2.rname:
                loci_npair_aligned[locus_name] += 1
                continue
            if verbose:
                print()

            pair_status = []
            pair_allele = []
            iread = 1
            for read in [r1, r2]:
                # Unaligned
                if not read.is_mapped():
                    pair_status.append('unaligned')
                elif is_wt(read, trim_max=trim_max):
                    pair_status.append('wt')
                else:
                    mutant = False
                    indels = get_indels(read)
                    if len(indels) > 0:
                        mutant = True
                        for etype, start, end in [indels[0], indels[-1]]:
                            if start <= loci_oligo_lengths[locus_name][0] + min_padding or loci_seq_lengths[locus_name] - loci_oligo_lengths[locus_name][1] - min_padding <= end:
                                mutant = False
                    if mutant:
                        pair_allele.append(indels)
                        pair_status.append('mutant')
                    else:
                        pair_status.append('aligned')
                    if verbose:
                        print('Read', iread, read.qname, read.cigar_string)
                        aln_ref, aln_read, aln_symbols = read.get_read_aln()
                        print(('0123456789' * math.ceil(len(aln_symbols) / 10.))[:len(aln_symbols)])
                        print(aln_ref)
                        print(aln_symbols)
                        print(aln_read)
                    iread += 1
            if verbose:
                print('Pair', pair_status, pair_allele)
            if pair_status.count('mutant') > 0:
                loci_npair_mutant[locus_name] += 1
                # Merging events: taking all read1 events and completing them with unique read2 events
                if len(pair_allele) > 1:
                    if r1.pos <= r2.pos:
                        # Add read1 events
                        pair_allele_merged = pair_allele[0]
                        # Add read2 events
                        # End of last indel detected on read1
                        end_last = pair_allele[0][-1][-1]
                        for etype, start, end in pair_allele[1]:
                            if end_last < start:
                                pair_allele_merged.append((etype, start, end))
                    else:
                        # Add read2 events
                        pair_allele_merged = []
                        # Start of first indel detected on read1
                        start_first = pair_allele[1][0][1]
                        for etype, start, end in pair_allele[1]:
                            if end < start_first:
                                pair_allele_merged.append((etype, start, end))
                        # Add read1 events
                        pair_allele_merged.extend(pair_allele[1])
                else:
                    pair_allele_merged = pair_allele[0]
                if verbose:
                    print('Pair merged', pair_allele_merged)
                # Adding allele
                if tuple(pair_allele_merged) in loci_alleles[locus_name]:
                    loci_alleles[locus_name][tuple(pair_allele_merged)] += 1
                else:
                    loci_alleles[locus_name][tuple(pair_allele_merged)] = 1
            elif pair_status.count('wt') > 0:
                loci_npair_wt[locus_name] += 1
            elif pair_status.count('aligned') > 0:
                loci_npair_aligned[locus_name] += 1
            loci_npair[locus_name] += 1

    return loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned
