# -*- coding: utf-8 -*-

#
# Copyright (C) 2015 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

def get_allele_aln(events, refseq):
    aln_ref = ''
    aln_read = ''
    aln_symbols = ''
    i = 0
    for etype, start, end in events:
        # Match block
        aln_ref += refseq[i:start]
        aln_read += refseq[i:start]
        aln_symbols += '|' * (start - i)
        # Deletion block
        if etype == 'D':
            aln_ref += refseq[start:end]
            aln_read += '-' * (end - start)
            aln_symbols += '.' * (end - start)
            i = end
        elif etype == 'I':
            aln_ref += '-' * (end - start)
            aln_read += 'N' * (end - start)
            aln_symbols += '.' * (end - start)
            i = start
    # Last Match block
    if i < len(refseq):
        aln_ref += refseq[i:]
        aln_read += refseq[i:]
        aln_symbols += '|' * len(refseq[i:])
    return aln_ref, aln_symbols, aln_read

def write_report(sample, loci, output, loci_alleles, loci_npair, loci_npair_mutant, loci_npair_aligned, loci_npair_wt, npair_unaligned, min_allele_percent, min_allele_reads):
    output.write('Sample {}\nUnaligned pair(s): {:,}\n\n'.format(sample, npair_unaligned))    
    # Per locus
    for locus in loci:
        if locus.name in loci_alleles:
            # Global results
            output.write('{} (# pair)\n{:>10} {:>2.0f}% {:,}\n{:>10} {:>2.0f}% {:,}\n{:>10} {:>2.0f}% {:,}\n{:>10}     {:,}\n\nAllele(s)\n'.format(locus.name, 'Mutant', loci_npair_mutant[locus.name]/loci_npair[locus.name]*100., loci_npair_mutant[locus.name], 'WT', loci_npair_wt[locus.name]/loci_npair[locus.name]*100., loci_npair_wt[locus.name], 'Aligned', loci_npair_aligned[locus.name]/loci_npair[locus.name]*100., loci_npair_aligned[locus.name], 'Total', loci_npair[locus.name]))
            # Alleles
            alleles = []
            for allele, allele_count in loci_alleles[locus.name].items():
                allele_percent = allele_count / (loci_npair_wt[locus.name] + loci_npair_mutant[locus.name]) * 100.
                if allele_percent > min_allele_percent and allele_count > min_allele_reads:
                    alleles.append((allele, allele_count, allele_percent))
            alleles.sort(key=lambda l: l[2], reverse=True)
            for events, allele_count, allele_percent in alleles:
                output.write('%.2f%% %s pair(s)\n'%(allele_percent, allele_count))
                aln_ref, aln_symbols, aln_read = get_allele_aln(events, locus.seq)
                output.write('   ' + aln_ref + '\n')
                output.write('   ' + aln_symbols + '\n')
                output.write('   ' + aln_read + '\n')