# -*- coding: utf-8 -*-

#
# Copyright (C) 2014-2015 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

"""SAM parsing."""

import re

class AlignedRead(object):
    def __init__(self, sam_line):
        # Spliting line
        fields = sam_line.rstrip().split('\t')
        self.qname = fields[0]
        self.flag = int(fields[1])
        self.rname = fields[2]
        # SAM is 1-based => -1 to 0-based
        self.pos = int(fields[3]) - 1
        self.mapq = fields[4]
        self.cigar_string = fields[5]
        self.mate_rname = fields[6]
        self.mate_pos = int(fields[7])
        self.insert_size = int(fields[8])
        self.seq = fields[9]
        self.qual = fields[10]
        self.tags_string = fields[11:]
        # Parsing CIGAR
        self.cigar = [(op[-1], int(op[:-1])) for op in re.findall(r'(\d+\w)', self.cigar_string)]
        # Parsing tags
        self.tags = {}
        for f in self.tags_string:
            name, ftype, value = f.split(':')
            if ftype == 'i':
                self.tags[name] = int(value)
            else:
                self.tags[name] = value

    def is_mapped(self):
        return self.flag != 4

    def is_reverse(self):
        return (self.flag & 16) > 0

    def to_sam(self):
        # pos+1 for SAM conversion
        return '\t'.join(map(str, [self.qname, self.flag, self.rname, self.pos+1, self.mapq, self.cigar_string, self.mate_rname, self.mate_pos,  self.insert_size, self.seq, self.qual] + self.tags_string))

    def to_fastq(self):
        return '@%s\n%s\n+\n%s\n'%(self.qname, self.seq, self.qual)
    
    def get_read_aln(self, refseq=None):
        aln_ref = []
        aln_read = ''
        aln_symbols = []
        irefseq = 0
        ireadseq = 0
        for block_type, block_length in self.cigar:
            if block_type == 'M':
                aln_ref.extend(list(self.seq[irefseq:irefseq+block_length]))
                aln_read += self.seq[ireadseq:ireadseq+block_length]
                aln_symbols.extend(['|'] * block_length)
                irefseq += block_length
                ireadseq += block_length
            elif block_type == 'D' or block_type == 'N':
                aln_ref.extend(list('N' * block_length))
                aln_read += '-' * block_length
                aln_symbols.extend(['.'] * block_length)
            elif block_type == 'I':
                aln_ref.extend(list('-' * block_length))
                aln_read += self.seq[ireadseq:ireadseq+block_length]
                aln_symbols.extend(['.'] * block_length)
                irefseq += block_length
                ireadseq += block_length
            elif block_type == 'S':
                aln_ref.extend(list(' ' * block_length))
                aln_read += self.seq[ireadseq:ireadseq+block_length]
                aln_symbols.extend([' '] * block_length)
                irefseq += block_length
                ireadseq += block_length
        if refseq is None:
            iref = 0
            for block in self.parse_tag_md():
                while iref < len(aln_ref) and (aln_ref[iref] == '-' or aln_ref[iref] == ' '):
                    iref += 1
                if block[0] == '^':
                    for nt in block[1:]:
                        aln_ref[iref] = nt
                        iref += 1
                elif block.isalpha():
                    aln_ref[iref] = block
                    aln_symbols[iref] = 'X'
                    iref += 1
                else:
                    block_length = int(block)
                    jblock = 0
                    while jblock < block_length:
                        if aln_ref[iref] != '-' and aln_ref[iref] != ' ' and aln_symbols[iref] != '.':
                            jblock += 1
                        iref += 1
        else:
            raise NotImplementedError
        return ''.join(aln_ref), aln_read, ''.join(aln_symbols)

    def parse_tag_md(self):
        tag = self.tags['MD']
        blocks = []
        current = ''
        i = 0
        while i < len(tag):
            l = tag[i]
            if l == '^':
                if len(current) > 0:
                    blocks.append(current)
                current = ''
                current += l
                i += 1
                l = tag[i]
                while i < len(tag) and l.isalpha():
                    current += l
                    i += 1
                    l = tag[i]
                blocks.append(current)
                current = ''
                i -= 1
            elif l.isalpha():
                if len(current) > 0:
                    blocks.append(current)
                blocks.append(l)
                current = ''
            else:
                current += l
            i += 1
        if len(current) > 0:
            blocks.append(current)
        return blocks
