#!/bin/bash

set -o errexit
set -o pipefail

path_out=$1

if [ -z "$path_out" ] ; then
    echo "... <path>"
    exit 1
fi

mkdir "$path_out"
cp -pr "src/mutseq" "$path_out/"
cp -p "scripts/mutseq.py" "$path_out/__main__.py"
cd "$path_out"
zip -r mutseq.pyz *
